# |/usr/bin/env python3

"""Sort a list of formats according to their level of compression"""

import sys

# Ordered list of image formats, from lower to higher compression
# (assumed classification)
fordered: tuple = ("raw", "bmp", "tiff", "ppm", "eps", "tga",
                   "pdf", "svg", "png", "gif", "jpeg", "webp")


def lower_than(format1: str, format2: str) -> bool:
    """Find out if format1 is lower than format2
    Returns True if format1 is lower, False otherwise.
    A format is lower than other if it is earlier in the fordered list.
    """
    format1 = fordered.index(format1)
    format2 = fordered.index(format2)
    if format1 >= format2:
        return False
    else:
        return True

def find_lower_pos(formats: list, pivot: int) -> int:
    """Find the lower format in formats after pivot
    Returns the index of the lower format found"""
    format1 = pivot
    for format2 in range(pivot + 1, len(formats)):
        if formats[format1] != formats[format2]:
            var = lower_than(formats[format1], formats[format2])
            if not var:
                format1 = format2

    return format1


def sort_formats(formats: list) -> list:
    """Sort formats list
    Returns the sorted list of formats, according to their
    position in fordered"""

    sum1 = 0
    pivot = formats.index(formats[0])
    while True:
        sum1 += 1
        num = find_lower_pos(formats, pivot)
        pivote = formats[pivot]
        mas_bajo = formats[num]
        if sum1 != len(formats):
            var = lower_than(pivote, mas_bajo)
            if not var:
                formats[pivot], formats[num] = formats[num], formats[pivot]
                pivot += 1
        else:
            break

    return formats


def main():
    """Read command line arguments, and print them sorted
    Also, check if they are valid formats using the fordered tuple"""
    sum2 = 0
    formats: list = sys.argv[1:]
    for format in formats:
        if format not in fordered:
            sys.exit(f"Formato inválido: {format}")
    sorted_formats: list = sort_formats(formats)
    for format in sorted_formats:
        sum2 += 1
        if sum2 == len(sorted_formats):
            print(format)
        else:
            print(format, end=" ")


if __name__ == '__main__':
    main()